TITLE_GENERAL="Results of producer-consumer benchmark"
XLABEL="Number of processes"
YLABEL="Store events per second"

OUTPUT_FILE="prodcons-plot1.png"
echo -e "set terminal png enhanced giant size 640, 480\n set out '"$OUTPUT_FILE"'\n
		set title \""$TITLE_GENERAL"\"\n
		set xlabel \""$XLABEL"\"\n
		set ylabel \""$YLABEL"\"\n
		plot \""prodcons-plot-data1.txt"\" using 1:2 title \""Dessert \(Ubuntu\)"\" with lines, \
		     \""prodcons-plot-data1.txt"\" using 1:3 title \""Dessert \(Windows\)"\" with lines, \
		     \""prodcons-plot-data1.txt"\" using 1:4 title \""Armando \(Ubuntu\)"\" with lines, \
		     \""prodcons-plot-data1.txt"\" using 1:5 title \""Armando \(Windows\)"\" with lines, \
		     \""prodcons-plot-data1.txt"\" using 1:6 title \""SimPy \(Ubuntu\)"\" with lines, \
		     \""prodcons-plot-data1.txt"\" using 1:7 title \""SimPy \(Windows\)"\" with lines" | gnuplot
		     
TITLE_GENERAL="RAM usage of producer-consumer benchmark"
YLABEL="Megabytes"

OUTPUT_FILE="prodcons-plot2.png"
echo -e "set terminal png enhanced giant size 640, 480\n set out '"$OUTPUT_FILE"'\n
		set title \""$TITLE_GENERAL"\"\n
		set xlabel \""$XLABEL"\"\n
		set ylabel \""$YLABEL"\"\n
		set key left top\n
		plot \""prodcons-plot-data2.txt"\" using 1:2 title \""Dessert \(Ubuntu\)"\" with lines, \
		     \""prodcons-plot-data2.txt"\" using 1:3 title \""Dessert \(Windows\)"\" with lines, \
		     \""prodcons-plot-data2.txt"\" using 1:4 title \""SimPy \(Ubuntu\)"\" with lines" | gnuplot
		     
TITLE_GENERAL="Heap efficiency in producer-consumer benchmark"
YLABEL="Store events per second"

OUTPUT_FILE="prodcons-plot3.png"
echo -e "set terminal png enhanced giant size 640, 480\n set out '"$OUTPUT_FILE"'\n
		set title \""$TITLE_GENERAL"\"\n
		set xlabel \""$XLABEL"\"\n
		set ylabel \""$YLABEL"\"\n
		plot \""prodcons-plot-data3.txt"\" using 1:2 title \""Binary \(Ubuntu\)"\" with lines, \
		     \""prodcons-plot-data3.txt"\" using 1:3 title \""Binary \(Windows\)"\" with lines, \
		     \""prodcons-plot-data3.txt"\" using 1:4 title \""Binomial \(Ubuntu\)"\" with lines, \
		     \""prodcons-plot-data3.txt"\" using 1:5 title \""Binomial \(Windows\)"\" with lines, \
		     \""prodcons-plot-data3.txt"\" using 1:6 title \""Fibonacci \(Ubuntu\)"\" with lines, \
		     \""prodcons-plot-data3.txt"\" using 1:7 title \""Fibonacci \(Windows\)"\" with lines, \
		     \""prodcons-plot-data3.txt"\" using 1:8 title \""Pairing \(Ubuntu\)"\" with lines, \
		     \""prodcons-plot-data3.txt"\" using 1:9 title \""Pairing \(Windows\)"\" with lines" | gnuplot