TITLE_GENERAL="Results of timeout benchmark"
XLABEL="Number of processes"
YLABEL="Events per second"

OUTPUT_FILE="timeout-plot1.png"
echo -e "set terminal png enhanced giant size 640, 480\n set out '"$OUTPUT_FILE"'\n
		set title \""$TITLE_GENERAL"\"\n
		set xlabel \""$XLABEL"\"\n
		set ylabel \""$YLABEL"\"\n
		plot \""timeout-plot-data1.txt"\" using 1:2 title \""Dessert \(Ubuntu\)"\" with lines, \
		     \""timeout-plot-data1.txt"\" using 1:3 title \""Dessert \(Windows\)"\" with lines, \
		     \""timeout-plot-data1.txt"\" using 1:4 title \""Armando \(Ubuntu\)"\" with lines, \
		     \""timeout-plot-data1.txt"\" using 1:5 title \""Armando \(Windows\)"\" with lines, \
		     \""timeout-plot-data1.txt"\" using 1:6 title \""SimPy \(Ubuntu\)"\" with lines, \
		     \""timeout-plot-data1.txt"\" using 1:7 title \""SimPy \(Windows\)"\" with lines" | gnuplot
		     
TITLE_GENERAL="RAM usage of timeout benchmark"
YLABEL="Megabytes"

OUTPUT_FILE="timeout-plot2.png"
echo -e "set terminal png enhanced giant size 640, 480\n set out '"$OUTPUT_FILE"'\n
		set title \""$TITLE_GENERAL"\"\n
		set xlabel \""$XLABEL"\"\n
		set ylabel \""$YLABEL"\"\n
		set key left top\n
		plot \""timeout-plot-data2.txt"\" using 1:2 title \""Dessert \(Ubuntu\)"\" with lines, \
		     \""timeout-plot-data2.txt"\" using 1:3 title \""Dessert \(Windows\)"\" with lines, \
		     \""timeout-plot-data2.txt"\" using 1:4 title \""SimPy \(Ubuntu\)"\" with lines" | gnuplot
		     
TITLE_GENERAL="Heap efficiency in timeout benchmark"
YLABEL="Events per second"

OUTPUT_FILE="timeout-plot3.png"
echo -e "set terminal png enhanced giant size 640, 480\n set out '"$OUTPUT_FILE"'\n
		set title \""$TITLE_GENERAL"\"\n
		set xlabel \""$XLABEL"\"\n
		set ylabel \""$YLABEL"\"\n
		plot \""timeout-plot-data3.txt"\" using 1:2 title \""Binary \(Ubuntu\)"\" with lines, \
		     \""timeout-plot-data3.txt"\" using 1:3 title \""Binary \(Windows\)"\" with lines, \
		     \""timeout-plot-data3.txt"\" using 1:4 title \""Binomial \(Ubuntu\)"\" with lines, \
		     \""timeout-plot-data3.txt"\" using 1:5 title \""Binomial \(Windows\)"\" with lines, \
		     \""timeout-plot-data3.txt"\" using 1:6 title \""Fibonacci \(Ubuntu\)"\" with lines, \
		     \""timeout-plot-data3.txt"\" using 1:7 title \""Fibonacci \(Windows\)"\" with lines, \
		     \""timeout-plot-data3.txt"\" using 1:8 title \""Pairing \(Ubuntu\)"\" with lines, \
		     \""timeout-plot-data3.txt"\" using 1:9 title \""Pairing \(Windows\)"\" with lines" | gnuplot