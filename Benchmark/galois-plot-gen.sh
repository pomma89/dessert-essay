TITLE_GENERAL="Completion time for a P2P simulation"
XLABEL="Number of active machines"
YLABEL="Minutes"

OUTPUT_FILE="galois-plot1.png"
echo -e "set terminal png enhanced giant size 640, 480\n set out '"$OUTPUT_FILE"'\n
		set title \""$TITLE_GENERAL"\"\n
		set key left top\n
		set xlabel \""$XLABEL"\"\n
		set ylabel \""$YLABEL"\"\n
		plot \""galois-plot-data1.txt"\" using 1:2 title \""Dessert \(Ubuntu\)"\" with lines, \
		     \""galois-plot-data1.txt"\" using 1:3 title \""Dessert \(Windows\)"\" with lines, \
		     \""galois-plot-data1.txt"\" using 1:4 title \""Armando \(Ubuntu\)"\" with lines, \
		     \""galois-plot-data1.txt"\" using 1:5 title \""Armando \(Windows\)"\" with lines, \
		     \""galois-plot-data1.txt"\" using 1:6 title \""SimPy \(Ubuntu, with threads\)"\" with lines, \
		     \""galois-plot-data1.txt"\" using 1:7 title \""SimPy \(Windows, with threads\)"\" with lines, \
		     \""galois-plot-data1.txt"\" using 1:8 title \""SimPy \(Ubuntu, no threads\)"\" with lines, \
		     \""galois-plot-data1.txt"\" using 1:9 title \""SimPy \(Windows, no threads\)"\" with lines" | gnuplot
		     
OUTPUT_FILE="galois-plot2.png"
echo -e "set terminal png enhanced giant size 640, 480\n set out '"$OUTPUT_FILE"'\n
		set title \""$TITLE_GENERAL"\"\n
		set key left top\n
		set xlabel \""$XLABEL"\"\n
		set ylabel \""$YLABEL"\"\n
		plot \""galois-plot-data2.txt"\" using 1:2 title \""Dessert \(with checks\)"\" with lines, \
		     \""galois-plot-data2.txt"\" using 1:3 title \""Dessert \(without checks\)"\" with lines, \
		     \""galois-plot-data2.txt"\" using 1:4 title \""Armando \(with checks\)"\" with lines, \
		     \""galois-plot-data2.txt"\" using 1:5 title \""Armando \(without checks\)"\" with lines" | gnuplot