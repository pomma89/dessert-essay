Sim.CurrentTimeUnit <- TimeUnit.Minute
let avgIncomingT, avgServiceT = (3).Minutes(), (10).Minutes()
let queueCount = 3 // Numero sportelli
let bankCap, bankLvl = 20000.0, 2000.0 // Euro
let waitTally, servTally = Sim.NewTally(), Sim.NewTally()
let mutable totClients = 0

let client(env: IEnvironment, queue: IResource, 
           bank: IContainer, amount, get) = seq<IEvent> {
    use req = queue.Request()
    let s1 = env.Now
    yield upcast req
    waitTally.Observe(env.Now - s1)
    let s2 = env.Now
    yield upcast env.Timeout(env.Random.Exponential(1.0/avgServiceT))
    if get then yield upcast bank.Get(amount)
    else yield upcast bank.Put(amount)
    servTally.Observe(env.Now - s2)
}

let rec spawner(env: IEnvironment, queues: IResource list, 
                bank) = seq<IEvent> {
    yield upcast env.Timeout(env.Random.Exponential(1.0/avgIncomingT))
    let queue = queues.MinBy(fun q -> q.Count)
    let amount = float(env.Random.Next(50, 500))
    let get = env.Random.NextDouble() < 0.4
    env.Start(client(env, queue, bank, amount, get)) |> ignore
    totClients <- totClients + 1
    yield! spawner(env, queues, bank)
}

let run() =
    let env = Sim.NewEnvironment(seed = 21)
    let queues = [for x in 1 .. queueCount do 
                      yield env.NewResource(1)]
    let bank = env.NewContainer(bankCap, bankLvl)

    // Avvio della simulazione
    env.Start(spawner(env, queues, bank)) |> ignore
    env.Run(until = (5).Hours())

    // Raccolta dati statistici
    let lvl = bank.Level
    printfn "Finanze totali al tempo %.2f: %g" env.Now lvl
    printfn "Clienti entrati: %d" totClients
    printfn "Clienti serviti: %d" servTally.Count
    printfn "Tempo medio di attesa: %.2f" (waitTally.Mean())
    printfn "Tempo medio di servizio: %.2f" (servTally.Mean())