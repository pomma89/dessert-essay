var rand = new TRandom();

// Generiamo tre numeri casuali aventi una
// distribuzione esponenziale, con 5 come media.
Console.WriteLine("Exponential:");
foreach (var e in rand.ExponentialSamples(1.0/5.0).Take(3))
    Console.WriteLine(e);

// Generiamo tre numeri casuali aventi una
// distribuzione normale, con 7 come media
// e 0.5 come varianza.
Console.WriteLine("Normal:");
foreach (var n in rand.NormalSamples(7.0, 0.5).Take(3))
    Console.WriteLine(n);