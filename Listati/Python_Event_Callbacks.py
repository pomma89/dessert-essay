from simpy import *
  
def doFail(env, ev):
    yield env.timeout(5)
    ev.fail(Exception("NO"))
    
def myCallback(ev):
    print("Successo: '%s'; Valore: '%s'" % (ev.ok, ev.value))
    
def proc(env):
    ev1 = env.timeout(7, "SI")
    ev1.callbacks = [myCallback]
    yield ev1    
    ev2 = env.event()
    ev2.callbacks = [myCallback]
    env.start(doFail(env, ev2))
    try: yield ev2
    except Exception: pass

env = Environment()
env.start(proc(env))
env.run()