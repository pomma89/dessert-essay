Module EventCallbacks
    Iterator Function DoFail(env As IEnvironment, 
                             ev As IEvent(Of String)) 
        As IEnumerable(Of IEvent)
        Yield env.Timeout(5)
        ev.Fail("NO")
    End Function

    Sub MyCallback(ev As IEvent)
        Console.WriteLine("Successo: '{0}'; Valore: '{1}'", 
                          ev.Succeeded, ev.Value)
    End Sub

    Iterator Function Proc(env As IEnvironment) 
        As IEnumerable(Of IEvent)
        Dim ev1 = env.Timeout(7, "SI")
        ev1.Callbacks.Add(AddressOf MyCallback)
        Yield ev1
        Dim ev2 = env.Event(Of String)()
        ev2.Callbacks.Add(AddressOf MyCallback)
        env.Start(DoFail(env, ev2))
        Yield ev2
    End Function

    Sub Run()
        Dim env = Sim.NewEnvironment()
        env.Start(Proc(env))
        env.Run()
    End Sub
End Module