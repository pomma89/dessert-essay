let run() =
    let env = Sim.NewEnvironment(seed = 21)
    let queues = [for x in 1..queueCount do
                      yield env.NewResource(1)]
    let bank = env.NewContainer(bankCap, bankLvl)

    // Avvio della simulazione
    env.Start(spawner(env, queues, bank)) |> ignore
    env.Run(until = (5).Hours())

    // Raccolta dati statistici
    let lvl = bank.Level
    printfn "Finanze totali al tempo %.2f: %g" env.Now lvl
    printfn "Clienti entrati: %d" totClients
    printfn "Clienti serviti: %d" servTally.Count
    printfn "Tempo medio di attesa: %.2f" (waitTally.Mean())
    printfn "Tempo medio di servizio: %.2f" (servTally.Mean())