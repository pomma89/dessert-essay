static class MachineLoad {
    static readonly string[] Tasks = {"A", "B", "C"};
    static readonly double LoadTime;
    static readonly double WorkTime;
    static readonly double SimTime;

    static MachineLoad() {
        Sim.CurrentTimeUnit = TimeUnit.Minute;
        LoadTime = 5.Minutes();
        WorkTime = 25.Minutes();
        SimTime = 100.Minutes();
    }

    static IEnumerable<IEvent> Worker(IEnvironment env) {
        Console.WriteLine("{0}: Carico la macchina...", 
                          env.Now);
        yield return env.Timeout(LoadTime);
        env.Exit(env.Random.Choice(Tasks));
    }

    static IEnumerable<IEvent> Machine(IEnvironment env) {
        while (true) {
            var worker = env.Start(Worker(env));
            yield return worker;
            Console.WriteLine("{0}: Eseguo il comando {1}", 
                              env.Now, worker.Value);
            yield return env.Timeout(WorkTime);
        }
    }

    public static void Run() {
        var env = Sim.NewEnvironment(21);
        env.Start(Machine(env));
        env.Run(SimTime);
    }
}