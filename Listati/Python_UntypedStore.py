def consumer(env, stringStore):
    while True:
        msg = yield stringStore.get()
        print("First char of message: %c" % msg[0])
        # La prima iterazione � OK, stamper� "P".
        # Tuttavia, si avr� un crash per la seconda,
        # poich� un intero non � indicizzabile.

env = Environment()
stringStore = Store(env)
stringStore.put("PINO") # Una stringa � OK
stringStore.put(21) # Ops, abbiamo inserito un intero!
env.start(consumer(env, stringStore))
env.run()