interface IHeapHandle<out V, out P> {
    V Value { get; }
    P Priority { get; }
}

interface IRawHeap<V, P> 
    : ICollection<IHeapHandle<V, P>>
{
    IComparer<P> Comparer { get; }
    int MaxCapacity { get; }  
    IHeapHandle<V, P> Min { get; }
    P this[IHeapHandle<V, P> handle] { set; }
    
    IHeapHandle<V, P> Add(V val, P pr);
    
    void Merge<V2, P2>(IRawHeap<V2, P2> other) 
        where V2 : V where P2 : P;
    
    IHeapHandle<V, P> RemoveMin();
    
    P UpdatePriorityOf(IHeapHandle<V, P> handle, 
                       P newPr);
    
    V UpdateValue(IHeapHandle<V, P> handle, 
                  V newVal);
    
    IEnumerable<IReadOnlyTree<V, P>> ToReadOnlyForest();
}