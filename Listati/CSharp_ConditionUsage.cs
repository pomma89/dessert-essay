IEnumerable<IEvent> Process(IEnvironment env) {
    var t1 = env.Timeout(3);
    var t2 = env.Timeout(7);
    var c1 = env.AllOf(t1, t2);
    yield return c1;
    Console.WriteLine(env.Now);
    Console.WriteLine(c1.Value.ContainsKey(t1));
    Console.WriteLine(c1.Value.ContainsKey(t2));
    
    t1 = env.Timeout(3);
    t2 = env.Timeout(7);
    var c2 = env.Condition(
        t1, t2, 
        c => c.Ev1.Succeeded || c.Ev2.Succeeded
    );
    yield return c2;
    Console.WriteLine(env.Now);
    Console.WriteLine(c2.Value.ContainsKey(t1));
    Console.WriteLine(c2.Value.ContainsKey(t2));
}

void Run() {
    var env = Sim.NewEnvironment();
    env.Start(Process(env));
    env.Run();
}