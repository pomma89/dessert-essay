Type TW = typeof(IEnumerableOfTWrapper<IEvent>);
FieldInfo Generator = TW.GetField("enumerable",
                                  BindingFlags.Instance |
                                  BindingFlags.NonPublic);

IEnumerable GenTransformer(IEnumerable<IEvent> generator)
{
    if (generator is IEnumerableOfTWrapper<IEvent>)
        return Generator.GetValue(generator) as IEnumerable;
    // Caso speciale per il processo Until.
    return generator;
}