static class PastaCooking {
    static readonly double AvgCookTime;
    static readonly double StdCookTime;
    static readonly double SimTime;

    static PastaCooking() {
        Sim.CurrentTimeUnit = TimeUnit.Minute;
        AvgCookTime = 10.Minutes();
        StdCookTime = 1.Minutes();
        SimTime = 50.Minutes();
    }

    static IEnumerable<IEvent> PastaCook(IEnvironment env) {
        while (true) {
            var cookTime = env.Random.Normal(AvgCookTime, 
                                             StdCookTime);
            var fmt = "Pasta in cottura per {0} minuti";
            Console.WriteLine(fmt, cookTime);
            yield return env.Timeout(cookTime);
            if (cookTime < AvgCookTime - StdCookTime)
                Console.WriteLine("Pasta poco cotta!");
            else if (cookTime > AvgCookTime + StdCookTime)
                Console.WriteLine("Pasta troppo cotta...");
            else
                Console.WriteLine("Pasta ben cotta!!!");
        }
    }

    public static void Run() {
        var env = Sim.NewEnvironment(21);
        env.Start(PastaCook(env));
        env.Run(SimTime);
    }
}