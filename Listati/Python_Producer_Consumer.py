from random import *
from simpy import *

rand = Random(21)
def randInt(): return rand.randint(1, 20)

def producer(env, store):
    while True:
        yield env.timeout(randInt())
        item = randInt()
        yield store.put(item)
        print("%d: Prodotto un %d" % (env.now, item))

def consumer(env, store):
    while True:
        yield env.timeout(randInt())
        item = yield store.get()
        print("%d: Consumato un %d" % (env.now, item))

env = Environment()
store = Store(env, capacity=2)
env.start(producer(env, store))
env.start(producer(env, store))
env.start(consumer(env, store))
env.run(until=60)