let rec spawner(env: IEnvironment, queues: IResource list, 
                bank) = seq<IEvent> {
    yield upcast env.Timeout(env.Random.Exponential(1.0/avgIncomingT))
    let queue = queues.MinBy(fun q -> q.Count)
    let amount = float(env.Random.Next(50, 500))
    let get = env.Random.NextDouble() < 0.4
    env.Start(client(env, queue, bank, amount, get)) |> ignore
    totClients <- totClients + 1
    // Richiama ricorsivamente la funzione spawner.
    // Grazie all'ottimizzazione della ricorsione in coda,
    // questo equivale all'uso di un costrutto while.
    yield! spawner(env, queues, bank)
}