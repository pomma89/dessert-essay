from random import *
from simpy import *

rand = Random(21)
hitProb = 0.7
simTime = 100 # Secondi

def newTarget(env):
    delay = rand.uniform(1, 20)
    target = rand.choice(["Alieno", "Pollo", "Unicorno"])
    return env.timeout(delay, target)

def shooter(env):
    while True:
        tgt = yield newTarget(env)
        hit = rand.random();
        if hit < hitProb:
            print("%g: %s colpito, si!" % (env.now, tgt))
        else:
            print("%g: %s mancato, no..." % (env.now, tgt))

env = Environment()
env.start(shooter(env))
env.run(simTime)