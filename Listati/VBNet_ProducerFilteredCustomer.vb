Module ProducerFilteredConsumer
    Iterator Function Producer(env As IEnvironment, 
                      store As IFilterStore(Of Integer))
        As IEnumerable(Of IEvent)
        While True
            Yield env.Timeout(env.Random.Next(1, 20))
            Dim item = env.Random.Next(1, 20)
            Yield store.Put(item)
            Console.WriteLine("{0}: Prodotto un {1}", 
                              env.Now, item)
        End While
    End Function

   Iterator Function Consumer(env As IEnvironment, 
                     store As IFilterStore(Of Integer),
                     name As String,
                     filter As Predicate(Of Integer))
        As IEnumerable(Of IEvent)
        While True
            Yield env.Timeout(env.Random.Next(1, 20))
            Dim getEv = store.Get(filter)
            Yield getEv
            Console.WriteLine("{0}: {1}, consumato un {2}", 
                              env.Now, name, getEv.Value)
        End While
    End Function

    Sub Run()
        Dim env = Sim.NewEnvironment(21)
        Dim store = env.NewFilterStore (Of Integer)(2)
        env.Start(Producer(env, store))
        env.Start(Producer(env, store))
        env.Start(Consumer(env, store, "PARI", 
                           Function(i) i Mod 2 = 0))
        env.Start(Consumer(env, store, "DISPARI", 
                           Function(i) i Mod 2 = 1))
        env.Run(until := 60)
    End Sub
End Module