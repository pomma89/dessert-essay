IEnumerable<IEvent> Interrupter(IProcess victim) {
    victim.Interrupt("NOW");
    yield break;
}

IEnumerable<IEvent> Process(IEnvironment env) {           
    yield return env.Start(Interrupter(env.ActiveProcess));
    object cause;
    if (env.ActiveProcess.Interrupted(out cause))
        Console.WriteLine("Interrupted at: " + cause);       
    
    // Le seguenti istruzioni sono commentate, poich�
    // darebbero origine a un'eccezione. Infatti,
    // il secondo segnale di interrupt non sarebbe catturato.
    //
    // yield return env.Start(Interrupter(env.ActiveProcess));
    // yield return env.Timeout(5);
}

void Run() {
    var env = Sim.NewEnvironment();
    env.Start(Process(env), "INTERRUPTED");
    env.Run();
}