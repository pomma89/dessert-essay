using IEvents = System.Collections.Generic.IEnumerable<IEvent>;

static class PublicToilet {
    static readonly double AvgPersonArrival;
    static readonly double AvgTimeInToilet;
    static readonly double SimTime;

    static PublicToilet() {
        Sim.CurrentTimeUnit = TimeUnit.Minute;
        AvgPersonArrival = 1.Minutes();
        AvgTimeInToilet = 5.Minutes();
        SimTime = 10.Minutes();
    }

    static IEvents Person(IEnvironment env, 
                          string gender, 
                          IResource toilet) {
        using (var req = toilet.Request()) {
            yield return req;
            Console.WriteLine("{0:0.00}: {1} --> Bagno", 
                              env.Now, gender);
            var t = env.Random.Exponential(1.0/AvgTimeInToilet);
            yield return env.Timeout(t);
            Console.WriteLine("{0:0.00}: {1} <-- Bagno", 
                              env.Now, gender);
        }
    }

    static IEvents PersonGenerator(IEnvironment env) {
        var womenToilet = env.NewResource(1);
        var menToilet = env.NewResource(1);
        var count = 0;
        while (true) {
            var rnd = env.Random.NextDouble();
            var gn = ((rnd<0.5) ? "Donna" : "Uomo") + count++;
            var tt = (rnd<0.5) ? womenToilet : menToilet;
            Console.WriteLine("{0:0.00}: {1} in coda", 
                              env.Now, gn);
            env.Start(Person(env, gn, tt));
            var t = env.Random.Exponential(1.0/AvgPersonArrival);
            yield return env.Timeout(t);
        }
    }

    public static void Run() {
        var env = Sim.NewEnvironment(21);
        env.Start(PersonGenerator(env));
        env.Run(SimTime);
    }
}