SimEvent MoveHandler(SimProcess process, SimEvent oldTarget)
{
    var gen = process.Generator as PythonGenerator;
    if (gen != null) {
        dynamic next;
        object cause;
        if (Env.ActiveProcess.Interrupted(out cause)) {
            _interrupt.cause = cause;
            next = gen.@throw(_interrupt);
        } else if (oldTarget == null)
            next = gen.send(null);
        else if (oldTarget.Failed)
            next = gen.@throw(oldTarget.Value);
        else
            next = gen.send(oldTarget.Value);
        return (next as SimEvent) ?? Env.EndEvent;
    }
    // Caso speciale per il processo Until.
    var hasNext = process.Steps.MoveNext();
    Debug.Assert(hasNext);
    return process.Steps.Current as SimEvent;
}