from random import *
from simpy import *

rand = Random(63)
def randInt(): return rand.randint(1, 20)

def producer(env, store):
    while True:
        yield env.timeout(randInt())
        item = randInt()
        yield store.put(item)
        print("%d: Prodotto un %d" % (env.now, item))

def consumer(env, store, name, filter):
    while True:
        yield env.timeout(randInt())
        i = yield store.get(filter)
        print("%d: %s, consumato un %d" % (env.now, name, i))

env = Environment()
store = FilterStore(env, capacity=2)
env.start(producer(env, store))
env.start(producer(env, store))
env.start(consumer(env, store, "PARI", lambda i: i%2 == 0))
env.start(consumer(env, store, "DISPARI", lambda i: i%2 == 1))
env.run(until=40)