Iterator Function Process(env As IEnvironment) 
    As IEnumerable(Of IEvent)
    
    Dim t = env.Timeout(5, value:="A BORING EXAMPLE")
    Yield t
    ' t.Value � una stringa, perci� possiamo
    ' applicare i metodi di string su di esso.
    Console.WriteLine(t.Value.Substring(2, 6))
    Dim intStore = env.NewStore(Of Double)()
    intStore.Put(t.Delay)
    Dim getEv = intStore.Get()
    Yield getEv
    ' getEv.Value � un numero decimale, perci� lo
    ' possiamo moltiplicare per 2.5, come previsto.
    Console.WriteLine(getEv.Value * 2.5)
End Function

Sub Run()
    Dim env = Sim.NewEnvironment()
    env.Start(Process(env))
    env.Run()
End Sub