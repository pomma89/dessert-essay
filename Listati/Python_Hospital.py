from simpy import *

# Priorita' decrescenti, piu' una priorita' e' bassa
# prima il processo sara' posizionato nella coda.
RED, YELLOW, GREEN = 0, 1, 2

def person(env, name, code, hospital):
    with hospital.request(code) as req:
        yield req
        print("%s viene curato..." % name)  

env = Environment()
hospital = PriorityResource(env, capacity=2)
env.start(person(env, "Pino", YELLOW, hospital))
env.start(person(env, "Gino", GREEN, hospital))
env.start(person(env, "Nino", GREEN, hospital))
env.start(person(env, "Dino", YELLOW, hospital))
env.start(person(env, "Tino", RED, hospital))
env.run()