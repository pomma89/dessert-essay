from random import *
from simpy import *

rand = Random(21)
avgTravelTime = 20.0 # Minuti
breakTime = 50 # Minuti

def train(env):
    while True:
        try:
            time = rand.expovariate(1.0/avgTravelTime)
            print("Treno in viaggio per %g minuti" % time)
            yield env.timeout(time)
            print("Arrivo in stazione, attesa passeggeri")
            yield env.timeout(2)
        except Interrupt, i:
            print("Al minuto %g: %s" % (env.now, i.cause))
            break
        
def emergencyBrake(env, tr):
    yield env.timeout(breakTime)
    tr.interrupt("FRENO EMERGENZA")

env = Environment()
tr = env.start(train(env))
env.start(emergencyBrake(env, tr))
env.run()