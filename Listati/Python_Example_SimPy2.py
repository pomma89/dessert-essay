from SimPy.Simulation import *

class Car(Process):
    def run():
        while True:
            print('Start parking at %d' % now())
            parking_duration = 5.0
            yield hold, self, parking_duration
            
            print('Start driving at %d' % now())
            trip_duration = 2.0
            yield hold, self, parking_duration
            
initialize()
c = Car()
activate(c, c.run(), at=0.0)
simulate(until=15.0)