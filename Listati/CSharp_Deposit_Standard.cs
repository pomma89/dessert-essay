// Per rendere lo script pi� sintetico...
using IOP = System.InvalidOperationException;
using AOR = System.ArgumentOutOfRangeException;

class MyBank : IBank {
    public double Amount { get; private set; }
    public bool IsOpen { get; private set; }
    
    public void Deposit(double amount) {
        if (!IsOpen)
            throw new IOP("La banca � chiusa");
        if (amount <= 0)
            throw new AOR("Quantit� non positiva");
        Amount += amount;
    }
}