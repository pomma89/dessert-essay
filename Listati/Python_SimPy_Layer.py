import clr
clr.AddReferenceToFile("Armando.exe")

import sys
import Armando

class Interrupt(Exception):
    def __init__(self):
        self.cause = None

class Environment(object): 
    def __new__(cls):
        object.__new__(cls)
        moveHandler = Armando.CustomMoveHandler(Interrupt())
        return moveHandler.Env

class Store(object): 
    def __new__(cls, env, capacity = sys.maxint):
        object.__new__(cls)
        return env.new_store(capacity)