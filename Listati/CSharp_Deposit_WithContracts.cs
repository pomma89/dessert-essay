class MyBank : IBank {
    public double Amount { get; private set; }
    public bool IsOpen { get; private set; }
    
    public void Deposit(double amount) {
        Amount += amount;
    }
}