def prodConsBenchmark_Consumer(env, store, counter):
    while True:
        yield env.timeout(counter.randomDelay())
        yield store.get()
        counter.increment()

def prodConsBenchmark_Producer(env, store, counter):
    while True:
        yield env.timeout(counter.randomDelay())
        yield store.put(randomInt())
        counter.increment()