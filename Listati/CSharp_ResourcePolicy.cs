IEnumerable<IEvent> Getter(IStore<int> store, char id) {
    var getEv = store.Get();
    yield return getEv;
    Console.WriteLine("{0}: {1}", id, getEv.Value);
}

void Run() {
    var env = Sim.NewEnvironment(seed: 21);
    const int capacity = 10;
    // Gli oggetti vengono conservati in ordine "casuale".
    var store = env.NewStore<int>(capacity, WaitPolicy.FIFO, 
                                   WaitPolicy.FIFO, 
                                   WaitPolicy.Random);
    for (var i = 0; i < capacity; ++i)
        store.Put(i);
    for (var i = 0; i < capacity; ++i)
        env.Start(Getter(store, (char) ('A' + i)));
    env.Run();
}