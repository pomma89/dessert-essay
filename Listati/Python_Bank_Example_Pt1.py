from random import *
from simpy import *

rand = Random(21)
exp = lambda x: rand.expovariate(1.0/x)

avgIncomingT, avgServiceT = 3.0, 10.0 # Minuti
queueCount = 3 # Numero sportelli
bankCap, bankLvl = 20000, 2000 # Euro

# Usate per raccogliere dati statistici
totClients, totServedClients = 0, 0
totWaitT, totServT = 0.0, 0.0

env = Environment()
queues = [Resource(env, 1) for i in range(queueCount)]
bank = Container(env, bankCap, bankLvl)