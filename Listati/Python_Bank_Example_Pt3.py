def spawner(env, queues, bank):
    global totClients
    while True:
        yield env.timeout(exp(avgIncomingT))
        queue = min(queues, key=lambda q:len(q.queue))
        amount = rand.uniform(50, 500)
        get = rand.random() < 0.4
        env.start(client(env, queue, bank, amount, get))
        totClients += 1