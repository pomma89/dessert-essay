IEnumerable<IEvent> Process(IEnvironment env, char id) {
    Console.WriteLine("{0}: {1}", id, env.Now);
    yield break;
}

void Run() {
    var env = Sim.NewEnvironment();
    env.StartDelayed(Process(env, 'A'), 7);
    env.Start(Process(env, 'B'));
    env.Run();
}