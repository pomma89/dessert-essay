let client(env: IEnvironment, queue: IResource, 
           bank: IContainer, amount, get) = seq<IEvent> {
    use req = queue.Request()
    let s1 = env.Now
    yield upcast req
    waitTally.Observe(env.Now - s1)
    let s2 = env.Now
    yield upcast env.Timeout(env.Random.Exponential(1.0/avgServiceT))
    if get then yield upcast bank.Get(amount)
    else yield upcast bank.Put(amount)
    servTally.Observe(env.Now - s2)
}