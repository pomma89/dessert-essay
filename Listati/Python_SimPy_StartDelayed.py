from simpy import *
from simpy.util import *

def myProcess(env):
    # Codice del processo...
    
env = Environment()
p1 = start_delayed(env, myProcess(env), 7)
p2 = start_delayed(env, myProcess(env), 3)
p3 = env.start(myProcess(env))
# Ordine di esecuzione: p3, p2, p1