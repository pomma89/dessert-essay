static class ConditionTester {
    static IEnumerable<IEvent> AProcess(IEnvironment env) {
        yield return env.Timeout(7);
        env.Exit("VAL_P");
    }

    static IEnumerable<IEvent> CondTester(IEnvironment env) {
        var t1 = env.NamedTimeout(5, "VAL_T", "T");
        var aProc = env.Start(AProcess(env), name: "P");
        var cond = env.AllOf(t1, aProc);
        yield return cond;
        Console.WriteLine("ALL: {0}", cond.Value);
        
        var t2 = env.NamedTimeout(5, "VAL_T", "T");
        aProc = env.Start(AProcess(env), name: "P");
        cond = env.AnyOf(t2, aProc);
        yield return cond;
        Console.WriteLine("ANY: {0}", cond.Value);

        aProc = env.Start(AProcess(env), name: "P");
        var aTime = env.NamedTimeout(5, "VAL_T", "T");
        ConditionEval<ITimeout<string>, IProcess> pred =
            c => c.Ev1.Succeeded && c.Ev2.Succeeded && 
            c.Ev1.Value.Equals("VAL_T") && 
            c.Ev2.Value.Equals("VAL_P");
        cond = env.Condition(aTime, aProc, pred);
        yield return cond;
        Console.WriteLine("CUSTOM: {0}", cond.Value);
    }

    public static void Run() {
        var env = Sim.NewEnvironment();
        env.Start(CondTester(env));
        env.Run();
    }
}