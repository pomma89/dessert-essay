IEnumerable<IEvent> Process(IEnvironment env) {
    var c1 = env.Timeout(3).And(env.Timeout(5)).And(env.Timeout(7));
    yield return c1;
    Console.WriteLine(env.Now);
    Console.WriteLine(c1.Value.ContainsKey(c1.Ev1));
    Console.WriteLine(c1.Value.ContainsKey(c1.Ev2));
    Console.WriteLine(c1.Value.ContainsKey(c1.Ev3));
    var c2 = env.Timeout(7).Or(env.Timeout(5).Or(env.Timeout(3)));
    yield return c2;
    Console.WriteLine(env.Now);
    Console.WriteLine(c2.Value.ContainsKey(c2.Ev1));
    Console.WriteLine(c2.Value.ContainsKey(c2.Ev2));
    Console.WriteLine(c2.Value.ContainsKey(c2.Ev3));
}

void Run() {
    var env = Sim.NewEnvironment();
    env.Start(Process(env));
    env.Run();
}