from simpy import *

# Risulta utile, se non necessario,
# passare un'istanza dell'ambiente ai processi.
def myProcess(env):
    # Codice del processo, composto da una
    # serie di yield alternate ad altro codice.
    
env = Environment()
p1 = env.start(myProcess(env))
p2 = env.start(myProcess(env))
p3 = env.start(myProcess(env))
# p1, p2 e p3 saranno eseguiti esattamente
# nell'ordine con cui sono stati avviati.