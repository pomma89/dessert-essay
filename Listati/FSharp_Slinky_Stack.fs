open Slinky

// Si ottiene uno stack implementato
// su una lista linkata di tipo thin.
let ls = ListFactory.NewLinkedStack<string>()

ls.Push("terzo")
ls.Push("secondo")
ls.Push("primo")
// Stampa attesa: primo secondo terzo
for i in ls do printf "%s " i
printfn ""

ls.Pop() |> ignore
ls.Pop() |> ignore
// Stampa attesa: terzo
printfn "%s" ls.Top