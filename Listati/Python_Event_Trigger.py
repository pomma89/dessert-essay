from simpy import *

def doSucceed(env, ev):
    yield env.timeout(5)
    ev.succeed("SI :)")
  
def doFail(env, ev):
    yield env.timeout(5)
    ev.fail(Exception())
    
def proc(env):
    ev1 = env.event()
    env.start(doSucceed(env, ev1))
    print(yield ev1) 
    ev2 = env.event()
    env.start(doFail(env, ev2))
    try: yield ev2
    except Exception: print("NO :(")

env = Environment()
env.start(proc(env))
env.simulate()