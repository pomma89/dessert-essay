// Generiamo tre numeri casuali aventi una
// distribuzione esponenziale, con 5 come media.
var exponential = new ExponentialDistribution(1.0/5.0);
Console.WriteLine("Exponential:");
for (var i = 0; i < 3; ++i)
    Console.WriteLine(exponential.NextDouble());

// Generiamo tre numeri casuali aventi una
// distribuzione normale, con 7 come media
// e 0.5 come varianza.
var normal = new NormalDistribution(7.0, 0.5);
Console.WriteLine("Normal:");
for (var i = 0; i < 3; ++i)
    Console.WriteLine(normal.NextDouble());