from heapq import *

class Process:
    def __init__(self, gen, time):
        self.gen = gen
        self.time = time
        
    def __cmp__(self, other):
        return self.time.__cmp__(other.time)
        
class Environment:
    def __init__(self):
        self._agenda = []
        self._now = 0.0
    
    def start(self, gen):
        p = Process(gen, self._now)
        heappush(self._agenda, p)
        
    def simulate(self):
        while len(self._agenda) > 0:
            p = self._agenda[0]
            p.gen.next()
            # Modifica dello heap a seconda
            # dello stato di p dopo il next.