let boxCapacity = 1.0 // Litri
let glassCapacity = 0.25 // Litri
let mutable fillBox: IEvent<int> = null

let rec filler(env: IEnvironment, box: IContainer) = 
    seq<IEvent> {
    yield upcast box.Put(boxCapacity - box.Level)
    fillBox <- env.Event<int>()
    yield upcast fillBox
    let id = fillBox.Value
    printfn "%f: %d chiama tecnico" env.Now id
    yield! filler(env, box)
}

let drinker(env: IEnvironment, id, box: IContainer) = 
    seq<IEvent> {
    // Occorre controllare che l'evento fillBox non sia gia'
    // stato attivato, perche' attivarlo nuovamente
    // risulterebbe in una eccezione da parte di SimPy.
    if box.Level < glassCapacity && not fillBox.Succeeded then
        fillBox.Succeed(id)    
    yield upcast box.Get(glassCapacity)
    printfn "%f: %d ha bevuto!" env.Now id
}

let rec spawner(env: IEnvironment, box, nextId) = 
    seq<IEvent> {
    yield upcast env.Timeout(5.0)
    env.Start(drinker(env, nextId, box)) |> ignore
    yield! spawner(env, box, nextId+1)
}

let run() =
    let env = Sim.NewEnvironment()
    let box = env.NewContainer(capacity=boxCapacity)
    env.Start(filler(env, box)) |> ignore
    env.Start(spawner(env, box, 0)) |> ignore
    env.Run(until=31)