let memRec(env: IEnvironment, tally: ITally) = seq<IEvent> {
    while true do
        yield upcast env.Timeout(memRecFreq)
        let procMem = currProc.WorkingSet64
        let gcMem = GC.GetTotalMemory(false)
        tally.Observe(float((procMem+gcMem)/(1024L*1024L)))
}