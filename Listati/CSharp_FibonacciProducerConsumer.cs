IEnumerable<IEvent> FibonacciFunc(IEnvironment env, int n) {
    if (n <= 0) yield return env.Exit(0);
    else if (n == 1) yield return env.Exit(1);
    else {
        var call = env.Call<int>(FibonacciFunc(env, n - 1));
        yield return call;
        var n1 = call.Value;
        call = env.Call<int>(FibonacciFunc(env, n - 2));
        yield return call;
        var n2 = call.Value;
        yield return env.Exit(n1 + n2);
    }
}

IEnumerable<IEvent> Producer(IEnvironment env, 
                              IStore<int> store, int n)
{
    var call = env.Call<int>(FibonacciFunc(env, n));
    yield return call;
    store.Put(call.Value);
}

IEnumerable<IEvent> Consumer(IStore<int> store) {
    var getEv = store.Get();
    yield return getEv;
    Console.WriteLine(getEv.Value);
}

void Run() {
    const int count = 10;
    var env = Sim.NewEnvironment();
    var store = env.NewStore<int>();
    for (var i = 0; i < count; ++i) {
        env.Start(Producer(env, store, i));
        env.Start(Consumer(store));
    }
    env.Run();
}