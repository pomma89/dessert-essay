// Per rendere lo script pi� sintetico...
using IOP = System.InvalidOperationException;
using AOR = System.ArgumentOutOfRangeException;

class MyBank : IBank {
    public double Amount { get; private set; }
    public bool IsOpen { get; private set; }
    
    public void Deposit(double amount) {
        Raise<IOP>.IfNot(IsOpen, "La banca � chiusa");
        Raise<AOR>.If(amount <= 0, "Quantit� non positiva");
        Amount += amount;
    }
}
