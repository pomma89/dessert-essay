Module HospitalPreemption
    Const Red = 0
    Const Yellow = 1
    Const Green = 2

    Iterator Function Person(env As IEnvironment, 
                             code As Integer, 
                             hospital As IPreemptiveResource,
                             delay As Double) 
        As IEnumerable(Of IEvent)
        Yield env.Timeout(delay)
        Dim name = env.ActiveProcess.Name
        Using req = hospital.Request(code, (code = Red))
            Yield req
            Console.WriteLine("{0} viene curato...", name)
            Yield env.Timeout(7)
            Dim info As IPreemptionInfo = Nothing
            If env.ActiveProcess.Preempted(info) Then
                Console.WriteLine("{0} scavalcato da {1}",
                                  name, info.By)
            Else
                Console.WriteLine("Cure finite per {0}", name)
            End If
        End Using
    End Function

    Sub Run()
        Dim env = Sim.NewEnvironment()
        Dim hospital = env.NewPreemptiveResource(2)
        env.Start(Person(env, Yellow, hospital, 0), "Pino")
        env.Start(Person(env, Green, hospital, 0), "Gino")
        env.Start(Person(env, Green, hospital, 1), "Nino")
        env.Start(Person(env, Yellow, hospital, 1), "Dino")
        env.Start(Person(env, Red, hospital, 2), "Tino")
        env.Run()
    End Sub
End Module