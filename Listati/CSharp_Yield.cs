IEnumerable<int> Fibonacci() {
    int a = 1, b = 0;
    while (true) {
        yield return a;
        b = a + b;
        yield return b;
        a = a + b;
    }
}