using IEvents = System.Collections.Generic.IEnumerable<IEvent>;

static class TrainInterrupt {
    static readonly double AvgTravelTime;
    static readonly double BreakTime;

    static TrainInterrupt() {
        Sim.CurrentTimeUnit = TimeUnit.Minute;
        AvgTravelTime = 20.Minutes();
        BreakTime = 50.Minutes();
    }

    static IEvents Train(IEnvironment env) {
        object cause;
        string fmt;
        while (true) {
            var time = env.Random.Exponential(1.0/AvgTravelTime);
            fmt = "Treno in viaggio per {0:.00} minuti";
            Console.WriteLine(fmt, time);
            yield return env.Timeout(time);
            if (env.ActiveProcess.Interrupted(out cause)) 
                break;
            fmt = "Arrivo in stazione, attesa passeggeri";
            Console.WriteLine(fmt);
            yield return env.Timeout(2.Minutes());
            if (env.ActiveProcess.Interrupted(out cause))
                break;
        }
        fmt = "Al minuto {0:.00}: {1}";
        Console.WriteLine(fmt, env.Now, cause);
    }

    static IEvents EmergencyBrake(IEnvironment env, 
                                  IProcess train) {
        yield return env.Timeout(BreakTime);
        train.Interrupt("FRENO EMERGENZA");
    }

    public static void Run() {
        var env = Sim.NewEnvironment(21);
        var train = env.Start(Train(env));
        env.Start(EmergencyBrake(env, train));
        env.Run();
    }
}