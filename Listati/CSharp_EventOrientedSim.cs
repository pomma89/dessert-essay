// Rappresentazione sintetica di un evento.
// Il metodo Apply esegue l'attivit�
// dell'evento, come l'aggiunta in coda
// di un nuovo cliente della banca.
class Event {
    public double Time;
    public void Apply() { /* ... */ }
}

// Insieme ordinato dove vengono memorizzati gli eventi.
var eventSet = SortedSet<Event>();
AddSomeEvents(eventSet);
// Ciclo principale del simulatore.
while (eventSet.Count > 0)
    eventSet.RemoveMin().Apply();