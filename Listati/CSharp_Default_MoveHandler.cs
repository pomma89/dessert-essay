SimEvent DefaultMoveHandler(SimProcess process, 
                            SimEvent oldTarget)
{
    if (!process.Steps.MoveNext()) return EndEvent;
    // Dobbiamo controllare che l'evento restituito
    // sia stato generato da questa libreria,
    // poich� dobbiamo essere in grado di
    // convertirlo al tipo SimEvent.
    var newTarget = process.Steps.Current as SimEvent;
    Raise<ArgumentNullException>.IfIsNull(newTarget);
    return newTarget;
}