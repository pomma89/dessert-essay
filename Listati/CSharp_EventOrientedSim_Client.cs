class OnCustomerEntered : Event {
    public void Apply() {
        // La chiamata ad Enqueue, a tempo debito, aggiungerà
        // un'istanza di OnCustomerOutOfQueue all'event set.
        FindShortestQueue().Enqueue(TheCustomer);
    }
}

class OnCustomerOutOfQueue : Event {
    public void Apply() {
        State = BeingServed;
        EventSet.Add(new OnCustomerDone(), GetServiceTime());
    }
}

class OnCustomerDone : Event {
    public void Apply() {
        State = Served;
        RegisterStats();
    }
}