def storeUser(env):
    store = Store(env, capacity=1)
    # Lo store � vuoto, per cui la chiamata
    # sottostante sar� bloccante. Tuttavia,
    # supponendo che qualcuno metta degli oggetti
    # nello store, su item proprio ritroveremo uno
    # di quegli oggetti.
    item = yield store.get()