from simpy import *

boxCapacity = 1 # Litri
glassCapacity = 0.25 # Litri
fillBox = None

def filler(env, box):
    global fillBox
    while True:
        yield box.put(boxCapacity - box.level)
        fillBox = env.event()
        id = yield fillBox
        print("%d: %d chiama tecnico" % (env.now, id))

def drinker(env, id, box):
    # Occorre controllare che l'evento fillBox non sia gia'
    # stato attivato, perche' attivarlo nuovamente
    # risulterebbe in una eccezione da parte di SimPy.
    if box.level < glassCapacity and not fillBox.triggered:
        fillBox.succeed(id)    
    yield box.get(glassCapacity)
    print("%d: %d ha bevuto!" % (env.now, id))

def spawner(env, box):
    id = 0
    while True:
        yield env.timeout(5)
        env.start(drinker(env, id, box))
        id += 1

env = Environment()
box = Container(env, capacity=boxCapacity)
env.start(filler(env, box))
env.start(spawner(env, box))
env.run(until=31)