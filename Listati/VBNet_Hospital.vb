Public Module Hospital
    Const Red = 0
    Const Yellow = 1
    Const Green = 2

    Iterator Function Person(env As IEnvironment, 
                             name As String, code As Integer, 
                             hospital As IResource)
        As IEnumerable(Of IEvent)
        Using req = hospital.Request(code)
            Yield req
            Console.WriteLine("{0} viene curato...", name)
            Yield env.Timeout(5)
        End Using
    End Function

    Sub Run()
        Dim env = Sim.NewEnvironment()
        Dim hospital = env.NewResource(2, WaitPolicy.Priority)
        env.Start(Person(env, "Pino", Yellow, hospital))
        env.Start(Person(env, "Gino", Green, hospital))
        env.Start(Person(env, "Nino", Green, hospital))
        env.Start(Person(env, "Dino", Yellow, hospital))
        env.Start(Person(env, "Tino", Red, hospital))
        env.Run()
    End Sub
End Module