let rec auxFib a b = seq {
    yield a
    let c = a + b
    yield c
    yield! auxFib (a+c) c  
}

let fibonacci = auxFib 1 0