var rand = new TRandom();

// Generiamo tre numeri casuali aventi una
// distribuzione esponenziale, con 5 come media.
Console.WriteLine("Exponential:");
for (var i = 0; i < 3; ++i)
    Console.WriteLine(rand.Exponential(1.0/5.0));

// Generiamo tre numeri casuali aventi una
// distribuzione normale, con 7 come media
// e 0.5 come varianza.
Console.WriteLine("Normal:");
for (var i = 0; i < 3; ++i)
    Console.WriteLine(rand.Normal(7.0, 0.5));