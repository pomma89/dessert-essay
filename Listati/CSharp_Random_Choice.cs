var names = new[] {"Pino", "Gino", "Dino"};
var rand = new TRandom();

// Estraiamo un nome a caso da "names"
// e lo stampiamo sulla console.
Console.WriteLine("Nome scelto: {0}", rand.Choice(names));