def timeoutBenchmark_PEM(env, counter):
    while True:
        yield env.timeout(counter.randomDelay())
        counter.increment()