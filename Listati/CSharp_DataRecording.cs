IEnumerable<IEvent> Person(IEnvironment env, 
                            IResource queue, ITally rec)
{
    using (var req = queue.Request()) {
        var startTime = env.Now;
        yield return req;
        rec.Observe(env.Now - startTime);
        var workTime = env.Random.Next(3, 10);
        yield return env.Timeout(workTime);
    }
}

IEnumerable<IEvent> Spawner(IEnvironment env, ITally rec) {
    var queue = env.NewResource(2);
    while (true) {
        env.Start(Person(env, queue, rec));
        yield return env.Timeout(env.Random.Next(2, 5));
    }
}

void Run() {
    var env = Sim.NewEnvironment(seed: 42);
    var rec = env.NewTally();
    env.Start(Spawner(env, rec));
    env.Run(2*60);
    Console.WriteLine("Totale clienti: {0}", rec.Total());
    Console.WriteLine("Attesa media: {0:.0}", rec.Mean());
}