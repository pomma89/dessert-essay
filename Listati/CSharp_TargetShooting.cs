static class TargetShooting {
    const double HitProb = 0.7;
    const double SimTime = 100;

    static string[] Targets = {"Alieno", "Pollo", "Unicorno"};

    static ITimeout<string> NewTarget(IEnvironment env) {
        var delay = env.Random.DiscreteUniform(1, 20);
        var target = env.Random.Choice(Targets);
        return env.Timeout(delay, target);
    }

    static IEnumerable<IEvent> Shooter(IEnvironment env) {
        while (true) {
            var timeout = NewTarget(env);
            yield return timeout;
            var hit = env.Random.NextDouble();
            string fmt;
            if (hit < HitProb)
                fmt = "{0}: {1} colpito, si!";
            else
                fmt = "{0}: {1} mancato, no...";
            Console.WriteLine(fmt, env.Now, timeout.Value);
        }
    }

    public static void Run() {
        var env = Sim.NewEnvironment(21);
        env.Start(Shooter(env));
        env.Run(SimTime);
    }
}