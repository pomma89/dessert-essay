from simpy import *

# Priorita' decrescenti, piu' una priorita' e' bassa
# prima il processo sara' posizionato nella coda.
RED, YELLOW, GREEN = 0, 1, 2

def person(env, name, code, hospital, delay):
    yield env.timeout(delay)
    env.active_process.name = name
    preempt = code == RED
    with hospital.request(code, preempt) as req:      
        yield req
        print("%s viene curato..." % name)
        try: 
            yield env.timeout(7)
            print("Cure finite per %s" % name)
        except Interrupt, i:
            byName = i.cause.by.name
            print("%s scavalcato da %s" % (name, byName))

env = Environment()
hospital = PreemptiveResource(env, capacity=2)
env.start(person(env, "Pino", YELLOW, hospital, 0))
env.start(person(env, "Gino", GREEN, hospital, 0))
env.start(person(env, "Nino", GREEN, hospital, 1))
env.start(person(env, "Dino", YELLOW, hospital, 1))
env.start(person(env, "Tino", RED, hospital, 2))
env.run()