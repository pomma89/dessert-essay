IList<T> HeapSort<T>(IEnumerable<T> elems) 
    where T : IComparable<T>
{
    var heap = HeapFactory.NewBinaryHeap<T>();
    foreach (var elem in elems)
        heap.Add(elem);
    var list = new List<T>(heap.Count);
    while (heap.Count != 0)
        list.Add(heap.RemoveMin());
    return list;
}

var ints = HeapSort(new[] {9, 8, 7, 6, 5, 4, 3, 2, 1});
// Output atteso: 1 2 3 4 5 6 7 8 9
foreach (var i in ints)
    Console.Write(i + " "); 