open Dessert
open MoreLinq // Espone MinBy, usato dentro Spawner

Sim.CurrentTimeUnit <- TimeUnit.Minute
let avgIncomingT, avgServiceT = (3).Minutes(), (10).Minutes()
let queueCount = 3 // Numero sportelli
let bankCap, bankLvl = 20000.0, 2000.0 // Euro
let waitTally, servTally = Sim.NewTally(), Sim.NewTally()
let mutable totClients = 0