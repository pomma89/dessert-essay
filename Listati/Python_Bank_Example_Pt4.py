# Avvio della simulazione
env.start(spawner(env, queues, bank))
env.run(until=5*60)

# Raccolta dati statistici
lvl = bank.level
print("Finanze totali al tempo %.2f: %d" % (env.now, lvl))
print("Clienti entrati: %d" % totClients)
print("Clienti serviti: %d" % totServedClients)
avgWait = totWaitT/totServedClients
print("Tempo medio di attesa: %.2f" % avgWait)
avgServ = totServT/totServedClients
print("Tempo medio di servizio: %.2f" % avgServ)