from random import *
from simpy import *

rand = Random(21)
avgCookTime = 10 # Minuti
stdCookTime = 1 # Minuti
simTime = 50 # Minuti

def pastaCook(env):
    normal = rand.normalvariate
    while True:
        cookTime = normal(avgCookTime, stdCookTime)
        print("Pasta in cottura per %g minuti" % cookTime)
        yield env.timeout(cookTime)
        if cookTime < avgCookTime - stdCookTime:
            print("Pasta poco cotta!")
        elif cookTime > avgCookTime + stdCookTime:
            print("Pasta troppo cotta...")
        else:
            print("Pasta ben cotta!!!")

env = Environment()
env.start(pastaCook(env))
env.run(simTime)