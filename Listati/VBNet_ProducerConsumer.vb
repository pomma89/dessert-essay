Module ProducerConsumer
    Iterator Function Producer(env As IEnvironment, 
                               store As IStore(Of Integer))
        As IEnumerable(Of IEvent)
        While True
            Yield env.Timeout(env.Random.Next(1, 20))
            Dim item = env.Random.Next(1, 20)
            Yield store.Put(item)
            Console.WriteLine("{0}: Prodotto un {1}", 
                              env.Now, item)
        End While
    End Function

    Iterator Function Consumer(env As IEnvironment, 
                               store As IStore(Of Integer))
        As IEnumerable(Of IEvent)
        While True
            Yield env.Timeout(env.Random.Next(1, 20))
            Dim getEv = store.Get()
            Yield getEv
            Console.WriteLine("{0}: Consumato un {1}", 
                              env.Now, getEv.Value)
        End While
    End Function

    Sub Run()
        Dim env = Sim.NewEnvironment(21)
        Dim store = env.NewStore (Of Integer)(2)
        env.Start(Producer(env, store))
        env.Start(Producer(env, store))
        env.Start(Consumer(env, store))
        env.Run(until := 60)
    End Sub
End Module