// Per rendere lo script pi� sintetico...
using IOP = System.InvalidOperationException;
using AOR = System.ArgumentOutOfRangeException;

[ContractClassFor(typeof(IBank))]
abstract class BankContract {
    double Amount { 
        get {
            Contract.Ensures(Contract.Result<double>() >= 0);
            return default(double);
        }
    }
    
    // Non si definisce un particolare contratto
    abstract bool IsOpen { get; }
    
    void Deposit(double amount) {
        Contract.Requires<IOP>(IsOpen, 
                               "La banca � chiusa");
        Contract.Requires<AOR>(amount > 0, 
                               "Quantit� non positiva");
        Contract.Ensures(Amount == 
                         Contract.OldValue(Amount) + amount);
    }
}
