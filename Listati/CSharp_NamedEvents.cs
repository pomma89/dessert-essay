IEnumerable<IEvent> Process(IEnvironment env) {
    var a = env.NamedTimeout(3, "A");
    var b = env.NamedTimeout(5, "B");
    var c = env.NamedTimeout(7, "C");
    var cond = a.And(b.Or(c));
    yield return cond;
    Console.WriteLine(env.Now);
    foreach (var e in cond.Value.Keys)
        Console.WriteLine(e.Name);
}

void Run() {
    var env = Sim.NewEnvironment();
    env.Start(Process(env));
    env.Run();
}