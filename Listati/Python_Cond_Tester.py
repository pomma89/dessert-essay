from simpy import *
from simpy.util import *

def aProcess(env):
    yield env.timeout(7)
    env.exit("VAL_P")

def conditionTester(env):
    aProc = env.start(aProcess(env), "P")
    res = yield all_of([env.timeout(5, "VAL_T", "T"), aProc])
    print("ALL: %s" % res)
    
    aProc = env.start(aProcess(env), "P")
    res = yield any_of([env.timeout(5, "VAL_T", "T"), aProc])
    print("ANY: %s" % res)
    
    def customEval(events, values):
        return len(events) == len(values) \
           and values[events[0]] == "VAL_T" \
           and values[events[1]] == "VAL_P"
    
    aProc = env.start(aProcess(env), "P")
    aTime = env.timeout(5, "VAL_T", "T")
    res = yield Condition(env, customEval, [aTime, aProc]) 
    print("CUSTOM: %s" % res)

env = Environment()
env.start(conditionTester(env))
env.run()