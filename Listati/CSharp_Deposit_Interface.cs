interface IBank {
    /// <summary>
    ///   La cifra depositata nella banca.
    /// </summary>
    double Amount { get; }

    /// <summary>
    ///   Vero se la banca � aperta, falso altrimenti.
    /// </summary>    
    bool IsOpen { get; }

    /// <summary>
    ///   Deposita la quantit� data nella banca.
    /// </summary>
    /// <param name="amount">
    ///   Una quantit� positiva di denaro.
    /// </param>
    /// <exception cref="ArgumentOutOfRangeException">
    ///   La quantit� � zero o negativa.
    /// </exception>
    /// <exception cref="InvalidOperationException">
    ///   La banca � chiusa.
    /// </exception>
    void Deposit(double amount);
}