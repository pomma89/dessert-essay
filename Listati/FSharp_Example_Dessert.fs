open Dessert

let rec car(env: IEnvironment) = seq<IEvent> {
    printfn "Start parking at %g" env.Now
    let parkingDuration = 5.0
    yield upcast env.Timeout(parkingDuration)

    printfn "Start driving at %g" env.Now
    let tripDuration = 2.0
    yield upcast env.Timeout(tripDuration)

    // Esegue nuovamente lo stesso processo.
    // L'ottimizzazione della ricorsione di coda
    // rende il seguente comando simile a un "while true".
    yield! car(env)
}

let env = Sim.NewEnvironment()
env.Start(car(env)) |> ignore
env.Run(until = 15.0)