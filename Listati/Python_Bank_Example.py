rand = Random(21)
exp = lambda x: rand.expovariate(1.0/x)

avgIncomingT, avgServiceT = 3.0, 10.0 # Minuti
queueCount = 3 # Numero sportelli
bankCap, bankLvl = 20000, 2000 # Euro

totClients, totServedClients = 0, 0
totWaitT, totServT = 0.0, 0.0

def client(env, queue, bank, amount, get):
    global totServedClients, totWaitT, totServT
    with queue.request() as req:
        start = env.now
        yield req
        totWaitT += env.now - start
        start = env.now
        yield env.timeout(exp(avgServiceT))
        if get: yield bank.get(amount)
        else: yield bank.put(amount)
        totServT += env.now - start
        totServedClients += 1
        
def spawner(env, queues, bank):
    global totClients
    while True:
        yield env.timeout(exp(avgIncomingT))
        queue = min(queues, key=lambda q:len(q.queue))
        amount = rand.uniform(50, 500)
        get = rand.random() < 0.4
        env.start(client(env, queue, bank, amount, get))
        totClients += 1

env = Environment()
queues = [Resource(env, 1) for i in range(queueCount)]
bank = Container(env, bankCap, bankLvl)

env.start(spawner(env, queues, bank))
env.run(until=5*60)

lvl = bank.level
print("Finanze totali al tempo %.2f: %d" % (env.now, lvl))
print("Clienti entrati: %d" % totClients)
print("Clienti serviti: %d" % totServedClients)
avgWait = totWaitT/totServedClients
print("Tempo medio di attesa: %.2f" % avgWait)
avgServ = totServT/totServedClients
print("Tempo medio di servizio: %.2f" % avgServ)