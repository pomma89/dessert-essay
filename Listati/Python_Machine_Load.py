from random import *
from simpy import *

rand = Random(21)
loadTime = 5 # Minuti
workTime = 25 # Minuti
simTime = 100 # Minuti

def worker(env):
    print("%g: Carico la macchina..." % env.now)
    yield env.timeout(loadTime)
    env.exit(rand.choice(["A", "B", "C"]))
    
def machine(env):
    while True:
        cmd = yield env.start(worker(env))
        print("%g: Eseguo il compito %s" % (env.now, cmd))
        yield env.timeout(workTime)

env = Environment()
env.start(machine(env))
env.run(simTime)