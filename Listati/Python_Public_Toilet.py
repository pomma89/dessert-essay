from random import *
from simpy import *

rand = Random(21)
avgPersonArrival, avgTimeInToilet = 1.0, 5.0 # Minuti
simTime = 10 # Minuti
exponential = rand.expovariate

def person(env, gender, toilet):
    with toilet.request() as req:
        yield req
        print("%.2f: %s --> Bagno" % (env.now, gender))
        yield env.timeout(exponential(1.0/avgTimeInToilet))
        print("%.2f: %s <-- Bagno" % (env.now, gender))   

def personGenerator(env):
    womenTt, menTt = Resource(env, 1), Resource(env, 1)
    id = 0
    while True:
        isWoman = rand.random() < 0.5
        gender = ("Donna" if isWoman else "Uomo") + str(id)
        toilet = womenTt if isWoman else menTt
        id += 1
        print("%.2f: %s in coda" % (env.now, gender))
        env.start(person(env, gender, toilet))
        yield env.timeout(exponential(1.0/avgPersonArrival))

env = Environment()
env.start(personGenerator(env))
env.run(simTime)