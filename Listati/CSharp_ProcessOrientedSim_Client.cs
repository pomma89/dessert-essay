class Customer : Process {
    public void Run() {
        // Chiamata bloccante finch� non � il suo turno.
        FindShortestQueue().Enqueue(TheCustomer);
        State = BeingServed;
        Timeout(GetServiceTime());
        State = Served;
        RegisterStats();
    }
}