interface IHeap<V, P>
    : ICollection<IHeapHandle<V, P>>
{
    IComparer<P> Comparer { get; }   
    IEqualityComparer<V> EqualityComparer { get; }  
    int MaxCapacity { get; }    
    IHeapHandle<V, P> Min { get; }   
    P this[V val] { get; set; }

    void Add(V val, P pr);
    
    bool Contains(V val);
    
    bool Contains(V val, P pr);
    
    void Merge<V2, P2>(IHeap<V2, P2> other) 
        where V2 : V where P2 : P;
    
    P PriorityOf(V val);
    
    IHeapHandle<V, P> Remove(V val);
    
    IHeapHandle<V, P> RemoveMin();
    
    P Update(V val, V newVal, P newPr);
    
    P UpdatePriorityOf(V val, P newPr);
    
    void UpdateValue(V val, V newVal);
    
    IEnumerable<IReadOnlyTree<V, P>> ToReadOnlyForest();
}