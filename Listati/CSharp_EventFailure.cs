IEnumerable<IEvent> EventFailer(IEvent<string> ev) {
    ev.Fail("SOMETHING BAD HAPPENED");
    yield break;
}

IEnumerable<IEvent> Process(IEnvironment env) {
    var ev = env.Event<string>();
    env.Start(EventFailer(ev));
    yield return ev;
    if (ev.Failed) Console.WriteLine(ev.Value);
}

void Run() {
    var env = Sim.NewEnvironment();
    env.Start(Process(env));
    env.Run();
}