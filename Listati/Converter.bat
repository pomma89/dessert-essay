echo off
echo.

echo C# files
for %%f in (*.cs) do (
    echo Converting %%f...
    "C:\Program Files (x86)\Highlight\highlight.exe" -d . -O latex -S cs -j 2 -l -r -f %%f
)
echo.

echo F# files
for %%f in (*.fs) do (
    echo Converting %%f...
    "C:\Program Files (x86)\Highlight\highlight.exe" -d . -O latex -S fs -j 2 -l -r -f %%f
)
echo.

echo Python files
for %%f in (*.py) do (
    echo Converting %%f...
    "C:\Program Files (x86)\Highlight\highlight.exe" -d . -O latex -S py -j 2 -l -r -f %%f
)
echo.

echo VB.NET files
for %%f in (*.vb) do (
    echo Converting %%f...
    "C:\Program Files (x86)\Highlight\highlight.exe" -d . -O latex -S vb -j 2 -l -r -f %%f
)
echo.

echo Text files
for %%f in (*.txt) do (
    echo Converting %%f...
    "C:\Program Files (x86)\Highlight\highlight.exe" -d . -O latex -S txt -j 2 -l -r -f %%f
)
echo.
  
pause