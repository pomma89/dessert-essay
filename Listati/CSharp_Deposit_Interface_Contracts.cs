[ContractClass(typeof(BankContract))]
interface IBank {
    /// <summary>
    ///   ...
    /// </summary>
    [Pure]
    double Amount { get; }

    /// <summary>
    ///   ...
    /// </summary>    
    [Pure]
    bool IsOpen { get; }

    /// <summary>
    ///   ...
    /// </summary>
    void Deposit(double amount);
}