def client(env, queue, bank, amount, get):
    global totServedClients, totWaitT, totServT
    with queue.request() as req:
        start = env.now
        yield req
        totWaitT += env.now - start
        start = env.now
        yield env.timeout(exp(avgServiceT))
        if get: yield bank.get(amount)
        else: yield bank.put(amount)
        totServT += env.now - start
        totServedClients += 1