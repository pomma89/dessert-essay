Iterator Function Fibonacci() As IEnumerable(Of Integer)
    Dim a = 1
    Dim b = 0
    While True
        Yield a
        b = a + b
        Yield b
        a = a + b
    End While
End Function