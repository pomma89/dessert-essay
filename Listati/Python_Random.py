import random

# Generiamo tre numeri casuali aventi una
# distribuzione esponenziale, con 5 come media.
print("Exponential:")
for i in range(3):
    print(random.expovariate(1.0/5.0))

# Generiamo tre numeri casuali aventi una
# distribuzione normale, con 7 come media
# e 0.5 come varianza.
print("Normal:")
for i in range(3):
    print(random.normalvariate(7.0, 0.5))
