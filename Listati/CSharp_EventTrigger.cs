static class EventTrigger {
    static IEnumerable<IEvent> DoSucceed(IEnvironment env, 
                                          IEvent<object> ev)
    {
        yield return env.Timeout(5);
        ev.Succeed("SI :)");
    }

    static IEnumerable<IEvent> DoFail(IEnvironment env, 
                                       IEvent<object> ev)
    {
        yield return env.Timeout(5);
        ev.Fail("NO :(");
    }

    static IEnumerable<IEvent> Proc(IEnvironment env) {
        var ev1 = env.Event();
        env.Start(DoSucceed(env, ev1));
        yield return ev1;
        if (ev1.Succeeded)
            Console.WriteLine(ev1.Value);

        var ev2 = env.Event();
        env.Start(DoFail(env, ev2));
        yield return ev2;
        if (ev2.Failed)
            Console.WriteLine(ev2.Value);
    }

    public static void Run() {
        var env = Sim.NewEnvironment();
        env.Start(Proc(env));
        env.Run();
    }
}