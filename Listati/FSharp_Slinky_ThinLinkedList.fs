open Slinky

// Le liste di tipo "thin" hanno il minore consumo
// di memoria, ma offrono un numero esiguo di
// operazioni applicabili su di esse.
let tl = ListFactory.NewThinLinkedList<string>()

tl.Add("terzo")
tl.Add("secondo")
tl.Add("primo")
// Stampa attesa: primo secondo terzo
for i in tl do printf "%s " i
printfn ""

tl.RemoveFirst()
// Stampa attesa: secondo terzo
for i in tl do printf "%s " i
printfn ""

tl.AddFirst("primo")
tl.Remove("secondo") |> ignore
// Stampa attesa: primo terzo
for i in tl do printf "%s " i
printfn ""