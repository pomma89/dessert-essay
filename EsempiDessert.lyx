#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass extbook
\use_default_options true
\master Relazione.lyx
\maintain_unincluded_children false
\language italian
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Esempi della banca
\begin_inset CommandInset label
LatexCommand label
name "chap:bank-examples"

\end_inset


\end_layout

\begin_layout Standard
I listati 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:simpy-bank-example"

\end_inset

 e 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:dessert-bank-example"

\end_inset

 riportano per esteso gli esempi della banca mostrati rispettivamente nelle
 sezioni 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:simpy-bank-example"

\end_inset

 e 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:dessert-bank-example"

\end_inset

, in modo che il lettore possa avere una visione di insieme di quanto viene
 raccontato in tali sezioni.
\end_layout

\begin_layout Standard
Inoltre, ricordiamo che, nel caso si vogliano provare tali esempi per conto
 proprio, i loro codice è presente anche sul repository del progetto 
\begin_inset CommandInset citation
LatexCommand cite
key "dessert-github"

\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/Python_Bank_Example.py.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Esempio della banca, implementato in Python su SimPy.
\begin_inset CommandInset label
LatexCommand label
name "alg:simpy-bank-example"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/FSharp_Bank_Example.fs.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Esempio della banca, implementato in F# su Dessert.
\begin_inset CommandInset label
LatexCommand label
name "alg:dessert-bank-example"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Esempi per Dessert
\begin_inset CommandInset label
LatexCommand label
name "chap:dessert-examples"

\end_inset


\end_layout

\begin_layout Standard
In questa appendice riportiamo le traduzioni su Dessert di molti esempi
 per SimPy, i quali sono presenti nel capitolo 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:simpy"

\end_inset

.
 Le traduzioni sono state poste qui, piuttosto che nel capitolo dedicato
 a Dessert, per non appesantirne troppo la lettura.
\end_layout

\begin_layout Standard
Gli esempi non hanno bisogno di particolari commenti, poiché sono molto
 simili a quelli per SimPy, escluse le inevitabili differenze sintattiche.
\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/CSharp_PastaCooking.cs.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Traduzione su Dessert dell'esempio per SimPy mostrato in 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:simpy-pasta-cooking"

\end_inset

.
\begin_inset CommandInset label
LatexCommand label
name "alg:dessert-pasta-cooking"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/CSharp_TargetShooting.cs.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Traduzione su Dessert dell'esempio per SimPy mostrato in 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:simpy-target-shooting"

\end_inset

.
\begin_inset CommandInset label
LatexCommand label
name "alg:dessert-target-shooting"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/CSharp_MachineLoad.cs.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Traduzione su Dessert dell'esempio per SimPy mostrato in 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:simpy-machine-load"

\end_inset

.
\begin_inset CommandInset label
LatexCommand label
name "alg:dessert-machine-load"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/CSharp_EventTrigger.cs.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Traduzione su Dessert dell'esempio per SimPy mostrato in 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:simpy-event-trigger"

\end_inset

.
\begin_inset CommandInset label
LatexCommand label
name "alg:dessert-event-trigger"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/VBNet_EventCallbacks.vb.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Traduzione su Dessert dell'esempio per SimPy mostrato in 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:simpy-event-callbacks"

\end_inset

.
\begin_inset CommandInset label
LatexCommand label
name "alg:dessert-event-callbacks"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/CSharp_ConditionTester.cs.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Traduzione su Dessert dell'esempio per SimPy mostrato in 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:simpy-cond-tester"

\end_inset

.
\begin_inset CommandInset label
LatexCommand label
name "alg:dessert-cond-tester"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/CSharp_TrainInterrupt.cs.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Traduzione su Dessert dell'esempio per SimPy mostrato in 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:simpy-train-interrupt"

\end_inset

.
\begin_inset CommandInset label
LatexCommand label
name "alg:dessert-train-interrupt"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/CSharp_PublicToilet.cs.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Traduzione su Dessert dell'esempio per SimPy mostrato in 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:simpy-public-toilet"

\end_inset

.
\begin_inset CommandInset label
LatexCommand label
name "alg:dessert-public-toilet"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/VBNet_Hospital.vb.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Traduzione su Dessert dell'esempio per SimPy mostrato in 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:simpy-hospital"

\end_inset

.
\begin_inset CommandInset label
LatexCommand label
name "alg:dessert-hospital"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/VBNet_HospitalPreemption.vb.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Traduzione su Dessert dell'esempio per SimPy mostrato in 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:simpy-hospital-preemption"

\end_inset

.
\begin_inset CommandInset label
LatexCommand label
name "alg:dessert-hospital-preemption"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/VBNet_ProducerConsumer.vb.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Traduzione su Dessert dell'esempio per SimPy mostrato in 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:simpy-producer-consumer"

\end_inset

.
\begin_inset CommandInset label
LatexCommand label
name "alg:dessert-producer-consumer"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/VBNet_ProducerFilteredCustomer.vb.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Traduzione su Dessert dell'esempio per SimPy mostrato in 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:simpy-producer-consumer-filter"

\end_inset

.
\begin_inset CommandInset label
LatexCommand label
name "alg:dessert-producer-consumer-filter"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float algorithm
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset CommandInset include
LatexCommand input
filename "Listati/FSharp_WaterDrinkers.fs.tex"

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Traduzione su Dessert dell'esempio per SimPy mostrato in 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:simpy-water-drinkers"

\end_inset

.
\begin_inset CommandInset label
LatexCommand label
name "alg:dessert-water-drinkers"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
